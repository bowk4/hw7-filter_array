function filterBy(arr, typeDataArray) {
    return arr.filter(item => typeof item !== typeDataArray)
}

const firstArray = ['hello', 'world', 23, '23', null];
const filteredArray = filterBy(firstArray, 'string');
console.log(filteredArray);